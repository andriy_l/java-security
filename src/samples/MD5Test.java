package samples;

import java.security.*;
import java.math.*;

class MD5Test
{
 public static void main(String [] args) throws CloneNotSupportedException,NoSuchAlgorithmException
 {
 
 MessageDigest sha = MessageDigest.getInstance("SHA"); 
 MessageDigest md5 = MessageDigest.getInstance("MD5"); 

 byte b[] = args[0].getBytes();
 sha.update(b);
 md5.update(b);
// sha.digest();
// md5.digest();
 System.out.println("MD5:"+new BigInteger(1, md5.digest()).toString(16)  );
 System.out.println("SHA:"+new BigInteger(1, sha.digest()).toString(16)  );
 System.out.println("SHA:"+sha.toString());
 System.out.println("MD5:"+md5.toString());
 System.out.println("SHA:"+sha.toString());
 
 }

}
