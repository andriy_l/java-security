import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;

public class SSLServer {
    public static void main(String args[]) throws Exception
    {
        try{
        //Creaet a SSLServersocket
        SSLServerSocketFactory factory=(SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
        SSLServerSocket sslserversocket=(SSLServerSocket) factory.createServerSocket(1234);
        SSLSocket sslsocket=(SSLSocket) sslserversocket.accept();
        DataInputStream is=new DataInputStream(sslsocket.getInputStream());
        PrintStream os=new PrintStream(sslsocket.getOutputStream());
        while(true)  
        {
            String input=is.readUTF();
            String ketqua=input.toUpperCase();
            os.println(ketqua);
        }
        }
        catch(IOException e)
        {
           System.out.print(e);
        }
    }
} 
