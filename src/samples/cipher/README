javax.crypto.Cipher is a class that provides functionality of a cryptographic cipher for encryption and decryption.
It has the following major methods.

getInstance() - Returns a Cipher object of the specified transformation (algorithm plus options)
from the implementation of the specified provider. If provider is not specified,
the default implementation is used.
This is a static method.

init() - Initializes this cipher for the specified operation mode with the specified key or public key certificate.
Two important operation modes are Cipher.ENCRYPT_MODE and Cipher.DECRYPT_MODE.

update() - Feeds additional input data to this cipher and generates partial output data.

doFinal() - Feeds the last part of the input data to this cipher
and generates the last part of the output data.

getBlockSize() - Returns the block size of this cipher.

getAlgorithm() - Returns the algorithm name of this cipher.

getProvider() - Returns the provider as a Provider object of this cipher.

See the next section how to use the javax.crypto.Cipher class in a sample program.