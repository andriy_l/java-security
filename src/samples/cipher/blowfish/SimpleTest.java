package samples.cipher.blowfish;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import java.security.Key;

/**
 Create a Blowfish key, encrypts some text,prints the ciphertext, then decrypts the text
 */
public class SimpleTest {
    public static void main(String[] args) throws Exception {
        String text = "java2s";

        KeyGenerator keyGenerator = KeyGenerator.getInstance("Blowfish");
        keyGenerator.init(128);

        Key key = keyGenerator.generateKey();

        Cipher cipher = Cipher.getInstance("Blowfish/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, key);

        byte[] ciphertext = cipher.doFinal(text.getBytes("UTF8"));

        for (int i = 0; i < ciphertext.length; i++) {
            System.out.print(ciphertext[i] + " ");
        }
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] decryptedText = cipher.doFinal(ciphertext);

        System.out.println(new String(decryptedText, "UTF8"));
    }
}
