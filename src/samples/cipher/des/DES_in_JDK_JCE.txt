To use DES algorithm in the JDK JCE package, we need to use several JCE classes and follow the steps:

1. Creating a DESKeySpec object from a 8-byte key material by calling the javax.crypto.spec.DESKeySpec constructor.

2. Creating a SecretKeyFactory object with DES algorithm name, mode name, and padding name by calling the javax.crypto.SecretKeyFactory.getInstance() static method.

3. Converting the DESKeySpec object to a SecretKey object through the SecretKeyFactory object by calling the javax.crypto.SecretKeyFactory.generateSecret() method.

4. Creating a Cipher object with DES algorithm name, mode name, and padding name by calling the javax.crypto.Cipher.getInstance() static method.

5. Initializing the Cipher object with the SecretKey object by calling the javax.crypto.Cipher.init() method.

6. Ciphering through input data as byte arrays by calling the javax.crypto.Cipher.update() method.

7. Finalizing the ciphering process by calling the javax.crypto.Cipher.doFinal() method.